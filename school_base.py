from __future__ import absolute_import
from __future__ import unicode_literals

# Standard Library Imports Section:
import csv
from collections import defaultdict

# Constants
TOTAL_SCHOOLS = 'Total Schools'
SCHOOL_BY_STATE = 'Schools by State'
SCHOOL_BY_CITY = 'Schools by City'
SCHOOL_BY_METRO_LOCATE = 'Schools by Metro-centric locale'
COMMON_WORDS = ['school']
MAX_RESULTS = 3


class Header(object):
    NCESSCH = 'NCESSCH'
    LEAID = 'LEAID'
    LEANM05 = 'LEANM05'
    SCHNAM05 = 'SCHNAM05'
    LCITY05 = 'LCITY05'
    LSTATE05 = 'LSTATE05'
    LATCOD = 'LATCOD'
    LONCOD = 'LONCOD'
    MLOCALE = 'MLOCALE'
    ULOCALE = 'ULOCALE'
    status05 = 'status05'


SCHOOL_LEVEL_MUTUALLY_EXCLUSIVE_WORDS = ['high', 'middle', 'elem', 'elementary']


# Utilities

def _isEleminatedByMutuallyExclusiveWordPresense(fromList, searchableString):
    searchableStringSet = set(searchableString.split(' '))
    isMutuallyExclusiveWordPresent = False
    isMutuallyExclusiveWordFoundInSource = False
    wordFound = []
    for word in fromList:
        if word in SCHOOL_LEVEL_MUTUALLY_EXCLUSIVE_WORDS:
            isMutuallyExclusiveWordFoundInSource = True
            wordFound.append(word)

    if not isMutuallyExclusiveWordFoundInSource:
        return isMutuallyExclusiveWordPresent

    toSearchInSearchable = set(SCHOOL_LEVEL_MUTUALLY_EXCLUSIVE_WORDS) - set(wordFound)
    if searchableStringSet.intersection(toSearchInSearchable):
        isMutuallyExclusiveWordPresent = True

    return isMutuallyExclusiveWordPresent


def _listMatch(fromList, searchableString):
    forwardMatched = 0
    backwardMatched = 0
    matched = 0
    cleanList = list(set(fromList) - set(COMMON_WORDS))
    totalTokens = len(cleanList)
    weightedTotal = totalTokens * (totalTokens + 1) / 2
    for index in range(0, totalTokens):
        word = cleanList[index]
        if word in searchableString:
            forwardMatched += (totalTokens - index)
            backwardMatched += index
            matched += 1
    matchedPercent = (float(matched) / len(cleanList)) * 100.00
    forwardBiasPercent = (float(forwardMatched) / weightedTotal) * 100.00
    backwardBiasPercent = (float(backwardMatched) / weightedTotal) * 100.00
    return [matchedPercent, forwardBiasPercent, backwardBiasPercent]


class School(object):
    def __init__(self, schoolData):
        self.id = schoolData[Header.NCESSCH]
        self.leaid = schoolData[Header.LEAID]
        self.county = schoolData[Header.LEANM05]
        self.schoolName = schoolData[Header.SCHNAM05]
        self.city = schoolData[Header.LCITY05]
        self.state = schoolData[Header.LSTATE05]
        self.lat = schoolData[Header.LATCOD]
        self.long = schoolData[Header.LONCOD]
        self.mlocate = schoolData[Header.MLOCALE]
        self.ulocate = schoolData[Header.ULOCALE]
        self.status = schoolData[Header.status05]

    def __str__(self):
        return self.schoolName + ", " + self.city + ", " + self.state


class SchoolMaster(object):
    def __init__(self, schoolDataFileName):
        self.schoolDataFileName = schoolDataFileName
        self.schoolDict = {}
        self.schoolIndex = {}
        self.masterStats = {
            TOTAL_SCHOOLS: 0,
            SCHOOL_BY_STATE: defaultdict(int),
            SCHOOL_BY_METRO_LOCATE: defaultdict(int),
            SCHOOL_BY_CITY: defaultdict(int),
        }

    def initialize(self):
        with open(self.schoolDataFileName, 'r', encoding="ISO-8859-1") as fileHandle:
            # pass the file object to DictReader() to get the DictReader object
            csvDictReader = csv.DictReader(fileHandle)
            for row in csvDictReader:
                school = School(row)
                self.schoolDict[row[Header.NCESSCH]] = school
                shortSchoolName = row[Header.SCHNAM05].lower()
                for commonWord in COMMON_WORDS:
                    shortSchoolName = shortSchoolName.replace(commonWord, '')
                searchableString = shortSchoolName + ' ' + \
                                   row[Header.LCITY05].lower() + ' ' + \
                                   row[Header.LSTATE05].lower()
                self.schoolIndex[searchableString] = row[Header.NCESSCH]
                self.masterStats[TOTAL_SCHOOLS] += 1
                self.masterStats[SCHOOL_BY_STATE][row[Header.LSTATE05]] += 1
                self.masterStats[SCHOOL_BY_CITY][row[Header.LCITY05]] += 1
                self.masterStats[SCHOOL_BY_METRO_LOCATE][row[Header.MLOCALE]] += 1

    def getBestMatch(self, matchedResults):
        keyList = sorted(matchedResults.keys(), key=float, reverse=True)[0:MAX_RESULTS]
        result = []
        for key in keyList:
            result.extend(matchedResults[key])
        return result

    def searchSchool(self, searchString):
        # Best case
        if searchString in self.schoolIndex.keys():
            schoolNCESSCH = self.schoolIndex[searchString]
            school = self.schoolDict[schoolNCESSCH]
            return [school.__str__()]
        searchList = searchString.split(' ')
        resultsMatched = defaultdict(list)

        for searchableString in self.schoolIndex.keys():
            if _isEleminatedByMutuallyExclusiveWordPresense(searchList, searchableString):
                continue

            matchedPercent, forwardBiasPercent, backwardBiasPercent = _listMatch(searchList, searchableString)
            if matchedPercent == 0:
                continue
            resultsMatched[matchedPercent].append(self.schoolIndex[searchableString])
            resultsMatched[forwardBiasPercent].append(self.schoolIndex[searchableString])
            resultsMatched[backwardBiasPercent].append(self.schoolIndex[searchableString])

        finalResult = []
        for schoolId in self.getBestMatch(resultsMatched):
            if schoolId not in finalResult:
                finalResult.append(schoolId)

        resultStrings = []
        for schoolNCESSCH in finalResult[0:MAX_RESULTS]:
            resultStrings.append(self.schoolDict[schoolNCESSCH].__str__())
        return resultStrings


schoolMaster = SchoolMaster('/Users/sbasu/Downloads/sl051bai.csv')
schoolMaster.initialize()
