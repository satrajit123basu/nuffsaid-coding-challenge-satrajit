from __future__ import absolute_import
from __future__ import unicode_literals

# Local App Imports Section:
from .school_base import SCHOOL_BY_CITY
from .school_base import SCHOOL_BY_METRO_LOCATE
from .school_base import SCHOOL_BY_STATE
from .school_base import TOTAL_SCHOOLS
from .school_base import schoolMaster


def print_count():
    print("Total Schools: " + str(schoolMaster.masterStats[TOTAL_SCHOOLS]))
    print("Schools by State: ")
    for state in schoolMaster.masterStats[SCHOOL_BY_STATE]:
        print("{} : {}".format(state, schoolMaster.masterStats[SCHOOL_BY_STATE][state]))

    print("Schools by Metro-centric locale:")
    for metroLocate in schoolMaster.masterStats[SCHOOL_BY_METRO_LOCATE]:
        print("{} : {}".format(metroLocate, schoolMaster.masterStats[SCHOOL_BY_METRO_LOCATE][metroLocate]))

    cityWithmaxSchools = None
    maxSchoolByCity = 0
    citiesWithAtleastOneSchool = 0
    for city in schoolMaster.masterStats[SCHOOL_BY_CITY]:
        if schoolMaster.masterStats[SCHOOL_BY_CITY][city] > 0:
            citiesWithAtleastOneSchool += 1
        if schoolMaster.masterStats[SCHOOL_BY_CITY][city] > maxSchoolByCity:
            maxSchoolByCity = schoolMaster.masterStats[SCHOOL_BY_CITY][city]
            cityWithmaxSchools = city
    print("City with most schools: " + cityWithmaxSchools + "({} schools)".format(str(maxSchoolByCity)))
    print("Unique cities with at least one school: " + str(citiesWithAtleastOneSchool))
