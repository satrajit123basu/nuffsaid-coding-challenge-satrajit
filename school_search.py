from __future__ import absolute_import
from __future__ import unicode_literals

# Standard Library Imports Section:
from datetime import datetime

# Local App Imports Section:
from .school_base import schoolMaster


def search_schools(searchString):
    startTime = datetime.now()
    result = schoolMaster.searchSchool(searchString.lower())
    endTime = datetime.now()
    timetaken = (endTime - startTime).total_seconds()
    print("Results for {} (search took: 0.000s)".format(searchString, timetaken))
    for schoolString in result:
        print(schoolString)
